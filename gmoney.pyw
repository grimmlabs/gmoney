#!E:\gMoney\venv\Scripts\pythonw.exe
#
# Monitor prices of items on WoW AH and provide stats on that basis
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import sys

sys.path.insert(0, 'lib')

from lib import loggr
from lib import version

from lib.ui import core

log = loggr.init()


if __name__ == "__main__":
    app = core.gMoneyApp()
    app.MainLoop()

    log.info("Shutting down!")

