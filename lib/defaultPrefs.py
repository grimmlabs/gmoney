# !/usr/bin/python3
#
# defaultPrefs - Created 2/21/2018 5:34 PM
#
# Description goes here.
#
# (c) 2001 - 2018 @ Grimmlabs
#
# See license file for redistribution details. 
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

"""Define default preferences for first-run."""

_defaults = {
    'pos':   (-1, -1),   # window position
    'size':  (-1, -1),   # window size
    'debug': False,      # Debug mode or no
}


def default():
    return _defaults

