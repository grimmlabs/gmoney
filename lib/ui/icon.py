#
# icon - Created 1/8/2018 6:15 PM
#
# See license file for redistribution details.
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import wx.adv

from .. import loggr

log = loggr.init()

TBMenu_Quit = 1000
TBMenu_Open = 1001


def IconMenu():
    menu = wx.Menu()

    menu.Append(TBMenu_Open, "Open gMoney")
    menu.Append(TBMenu_Quit, "Quit")

    return menu


class TrayIcon(wx.adv.TaskBarIcon):
    def __init__(self):
        wx.adv.TaskBarIcon.__init__(self)

        icon = wx.Icon("assets/images/gold-15x15.png", type=wx.BITMAP_TYPE_PNG)

        if self.IsOk()	:
            self.SetIcon(icon=icon)

        self.Bind(wx.EVT_MENU, self.sortingHat)
        self.Bind(wx.adv.EVT_TASKBAR_LEFT_DCLICK, self.DoubleClick)

    def CreatePopupMenu(self):
        self.menu = IconMenu()
        return self.menu

    def sortingHat(self, evt):
        if evt.Id == TBMenu_Quit:
            wx.GetApp().CloseApp(evt)
        elif evt.Id == TBMenu_Open:
            wx.GetApp().openMainWindow()
        else:
            log.debug(f"Unhandled Menu event: [{evt.Id}]")

    def DoubleClick(self, evt):
        wx.GetApp().openMainWindow()
        # If the window is already open but hidden, this will show it.
        wx.FindWindowByName("gmoneyTopFrame").Raise()


