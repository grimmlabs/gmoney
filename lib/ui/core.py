# !/usr/bin/python3
#
# core - Created 1/8/2018 7:03 PM
#
# Core UI for Snipr
#
# (c) 2017 - 2020 @ Grimmlabs
#
# See license file for redistribution details.
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import json
import logging

import wx

from .. import config as cfg
from .. import defaultPrefs
from .. import version

from . import icon
from . import MainFrame

log = logging.getLogger(cfg.logName)


# noinspection PyAttributeOutsideInit
class gMoneyApp(wx.App):
    def OnInit(self):
        self.SetAppName(version.name)
        self.SetVendorName(cfg.vendorName)

        # Necessary because we won't always have frames open.
        self.SetExitOnFrameDelete(False)

        # Initializing the image handlers allows the program to
        # use images in far more formats than just bitmaps.
        wx.InitAllImageHandlers()

        self.prefs = self.loadConfig()
        self.Tray = icon.TrayIcon()

        frame = MainFrame.mainFrame()
        self.SetTopWindow(frame)
        # Frame created, but not shown, at startup

        # # Initial update
        # if not wx.GetApp().prefs['debug']:
        #     StartCoroutine(update.do_update, frame)
        # else:
        #     log.critical("Debug mode enabled.")

        return True

    def loadConfig(self):
        """Load config file from disk"""

        try:
            with open('ggeometry.json') as fp:
                prefs = json.load(fp)
        except FileNotFoundError as e:
            prefs = defaultPrefs.default()

        return prefs

    def CloseApp(self, event):
        """Kill off the program. Hasta la vista, bye bye ..."""
        self.Tray.Destroy()

        # Due to the funky way that things interact here, we have to kill the app off in
        # little bits. ExitMainLoop stops processing of the app, then we have to 'bump'
        # the event processor to get it to close all open windows.
        self.ExitMainLoop()
        wx.WakeUpIdle()

    def openMainWindow(self):
        """Shows main window (normally hidden)"""
        frame = wx.FindWindowByName("gmoneyTopFrame")
        frame.Show(True)

