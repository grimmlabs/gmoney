#
# MainFrame - Created 1/12/2018 5:53 PM
#
# Main UI frame for gMoney
#
# See license file for redistribution details.
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import datetime
import json
import logging
import sys

import wx

from .. import blizz
from .. import config as cfg
from .. import update
from .. import version

from .Pages import logger as loggr
from .Pages import controlPanel as control

from .Pages import herbTab
from .Pages import miscTab
from .Pages import tomeTab
from .Pages import enchTab
from .Pages import potTab
from .Pages import gemTab
from .Pages import scribeTab
from .Pages import tailorTab

log = logging.getLogger('gmoney')


def EVT_BLIZZ_RESULT(win, func):
    win.Connect(-1, -1, blizz.EVT_BLIZZ_RESULT_ID, func)


def EVT_PROC_RESULT(win, func):
    win.Connect(-1, -1, update.EVT_PROCESS_RESULT_ID, func)


class SnoteBook(wx.Notebook):
    def __init__(self, parent):
        wx.Notebook.__init__(self, parent, -1, style=wx.NB_TOP | wx.NB_MULTILINE)

        self.AddPage(tailorTab.clothPanel(self), "Cloth")
        self.AddPage(enchTab.enchPanel(self), "Enchanting")
        self.AddPage(gemTab.gemPanel(self), "Gems")
        self.AddPage(herbTab.herbPanel(self), "Herbs")
        self.AddPage(miscTab.miscPanel(self), "Misc SL")
        self.AddPage(potTab.potPanel(self), "Potions")
        self.AddPage(scribeTab.scribePanel(self), "Scribe")
        self.AddPage(tomeTab.tomePanel(self), "Tomes")

        self.AddPage(loggr.LoggerPanel(self), "Log")

        self.AddPage(control.Control(self), "Control")


class mainFrame(wx.Frame):
    """
    All this is for is a container around the windows that are contained within the notebook control.
    Additionally, this serves as a central point of authority for things like the historyDB et al. Basically,
    if you can get a reference to this window, you can gain access to its treasures.
    """
    def __init__(self):
        wx.Frame.__init__(self, None, title=version.splash, name="gmoneyTopFrame")

        self.SetIcon(wx.Icon("assets/images/gold-15x15.png", wx.BITMAP_TYPE_PNG))

        self.status = self.CreateStatusBar(2, style=wx.STB_SIZEGRIP)

        self.nb = SnoteBook(self)
        self.Bind(wx.EVT_CLOSE, self.onClose)

        self.prefs = wx.GetApp().prefs
        self.SetPosition(self.prefs['pos'])
        self.SetSize(self.prefs['size'])

        self.SetStatusText(version.fullVersion, 0)

        # The difference between lastModified and lastUpdated is the former is taken from the API headers
        # returned from Blizz, and the latter reflects the time that we finished processing the last update.
        self.lastModified = None
        self.lastUpdated = 0
        self.access_token = ""      # Get this from Blizz
        self.worker = None          # If not None, means there is currently a running task
        self.historyDB = None       # HistoryDB contains a historical record of auctions
        self.dlg = None             # Dialog that will show progress during updates

        # Handle update results
        EVT_BLIZZ_RESULT(self, self.onBlizzResults)
        EVT_PROC_RESULT(self, self.onProcessed)

        log.info(f"{version.name} {version.version} Starting up")
        log.info(f"Using Python {sys.version}.")

        # Load the history DB and item cache
        self.loadHistory()
        # itemCache is a local cache of items fetched from Blizz.
        self.itemCache = self.loadCache()
        self.cacheDirty = False  # True = we updated, need to save.

        log.info(f"Refresh interval is [{cfg.refreshInterval}] minutes.")

        wx.CallAfter(self.updateStatus)
        wx.CallLater(0, blizz.updateFromBlizz, self)

    def onClose(self, evt):
        """Close the frame (do not exit program)"""
        log.debug("Hiding mainFrame")
        self.Show(False)
        size = self.GetSize()
        pos = self.GetPosition()

        prefs = wx.GetApp().prefs

        prefs['size'] = [size.x, size.y]
        prefs['pos'] = [pos.x, pos.y]

        with open(cfg.geometryFile, 'w') as fp:
            json.dump(prefs, fp)

    def updateStatus(self):
        """Update the window's status text"""
        if self.lastUpdated:
            self.SetStatusText(f"Last update: {self.lastUpdated}", 1)
        else:
            self.SetStatusText(f"Updating ...", 1)

    def onBlizzResults(self, evt):
        """Called when we've finished fetching results from Blizz"""
        results = evt.data

        if results:
            log.info("Processing data.")
            wx.CallLater(0, update.do_update, self, results)
        else:
            log.info("... Going back to sleep.")
            wx.CallLater((cfg.refreshInterval * 60 * 1000), self.scheduleUpdate)

        self.worker = None

    def onProcessed(self, evt):
        """Called when we've finished proceessing new data from Blizz"""

        # Update all the different windows
        wx.CallAfter(wx.FindWindowByName("tailorTab").refresh)
        wx.CallAfter(wx.FindWindowByName("enchTab").refresh)
        wx.CallAfter(wx.FindWindowByName("gemTab").refresh)
        wx.CallAfter(wx.FindWindowByName("herbGrid").populate)
        wx.CallAfter(wx.FindWindowByName("miscGrid").populate)
        wx.CallAfter(wx.FindWindowByName("potPanel").refresh)
        wx.CallAfter(wx.FindWindowByName("scribeTab").refresh)
        wx.CallAfter(wx.FindWindowByName("tomeTab").refresh)

        # Update status bar
        wx.CallAfter(wx.FindWindowByName("gmoneyTopFrame").updateStatus)
        # Schedule next update
        wx.CallLater((cfg.refreshInterval * 60 * 1000), self.scheduleUpdate)

        # If not set to None, we'll never poll again
        self.worker = None

        # Save cache if it was recently updated.
        if self.cacheDirty:
            self.serializeCache()

        # Finally, indicate when we last updated our data
        self.lastUpdated = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    def loadHistory(self):
        """Load history from disk"""
        try:
            with open(cfg.historyDB) as fp:
                self.historyDB = json.load(fp)
                log.info(f"Loaded history DB from disk, {len(self.historyDB)} items loaded.")
        except FileNotFoundError:
            self.historyDB = {}
            log.debug("Initializing new history DB.")

    def scheduleUpdate(self):
        """Time to see if Blizz has something for us."""
        log.debug("vvvvvvvvvvvvvvvvvvvvvv")
        log.debug("The beast awakens ...")
        log.debug("^^^^^^^^^^^^^^^^^^^^^")
        wx.CallLater(0, blizz.updateFromBlizz, self)

    def loadCache(self):
        """Load the cache from disk"""
        rc = {}

        try:
            with open(cfg.cacheFile) as fp:
                rc = json.load(fp)
                log.info(f"Loaded cache from disk, {len(rc)} items loaded.")
        except FileNotFoundError:
            log.debug("Initializing fresh cache.")

        return rc

    def updateCache(self, item):
        """Add an item to the cache"""
        # i = item

        self.itemCache[item['id']] = item
        self.cacheDirty = True

    def serializeCache(self):
        """Save the cache out to disk if it's been updated."""

        with open(cfg.cacheFile, "w") as fp:
            json.dump(self.itemCache, fp, indent=4)
            log.info(f"Updated cache on disk, [{len(self.itemCache)}] items serialized.")
