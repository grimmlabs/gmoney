#
# (c) 2017 - 2020 @ Grimmlabs
#
# See license file for redistribution details. 
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import wx

from . import gridBase


class jcGrid(gridBase.recipeGrid):
    def __init__(self, parent):
        gridBase.recipeGrid.__init__(self, parent, widgetName="jcGrid", itemKind="jc")


class gemGrid(gridBase.matGrid):
    def __init__(self, parent):
        gridBase.matGrid.__init__(self, parent, widgetName="gemMatGrid", matKind="jc-mats")


class gemMetalGrid(gridBase.matGrid):
    def __init__(self, parent):
        gridBase.matGrid.__init__(self, parent, widgetName="gemMetalGrid", matKind="metals")


class gemPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, name='gemTab')

        self.subs = []

        f = self.GetFont()
        f.SetWeight(wx.FONTWEIGHT_BOLD)
        f.SetPointSize(f.GetPointSize() + 2)

        sizer = wx.BoxSizer(wx.VERTICAL)

        t = wx.StaticText(self, -1, "JC Products")
        t.SetFont(f)
        sizer.Add(t, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)

        g = jcGrid(self)
        sizer.Add(g, proportion=1, border=3, flag=wx.ALL | wx.EXPAND)
        self.subs.append(g)

        l = wx.StaticLine(self)
        sizer.Add(l, border=5, flag=wx.ALL | wx.EXPAND)

        t = wx.StaticText(self, -1, "Raw Gems")
        t.SetFont(f)
        sizer.Add(t, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)

        g = gemGrid(self)
        sizer.Add(g, proportion=1, border=3, flag=wx.ALL | wx.EXPAND)
        self.subs.append(g)

        l = wx.StaticLine(self)
        sizer.Add(l, border=5, flag=wx.ALL | wx.EXPAND)

        t = wx.StaticText(self, -1, "Metals")
        t.SetFont(f)
        sizer.Add(t, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)

        g = gemMetalGrid(self)
        sizer.Add(g, proportion=1, border=3, flag=wx.ALL | wx.EXPAND)
        self.subs.append(g)

        self.SetSizer(sizer)
        sizer.Fit(self)

    def refresh(self):
        for g in self.subs:
            wx.CallAfter(g.populate)
