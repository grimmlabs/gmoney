#
# (c) 2017 - 2020 @ Grimmlabs
#
# See license file for redistribution details. 
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import wx

from . import gridBase


class herbGrid(gridBase.matGrid):
    def __init__(self, parent):
        gridBase.matGrid.__init__(self, parent, widgetName="herbGrid", matKind="herb")


class herbPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        g = herbGrid(self)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(g, proportion=1, flag=wx.ALL | wx.EXPAND, border=3)

        # debugging button
        # b = wx.Button(self, -1, label="Refresh")
        # sizer.Add(b)
        # b.Bind(wx.EVT_BUTTON, g.onButtonMash)
        # end debugging button

        self.SetSizer(sizer)
        sizer.Fit(self)

