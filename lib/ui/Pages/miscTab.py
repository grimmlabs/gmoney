#
# (c) 2017 - 2020 @ Grimmlabs
#
# See license file for redistribution details. 
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import wx

from . import gridBase


class miscGrid(gridBase.matGrid):
    def __init__(self, parent):
        gridBase.matGrid.__init__(self, parent, widgetName="miscGrid", matKind="sl")


class miscPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        g = miscGrid(self)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(g, proportion=1, border=3, flag=wx.ALL | wx.EXPAND)

        # debugging button
        # b = wx.Button(self, -1, label="Refresh")
        # sizer.Add(b)
        # b.Bind(wx.EVT_BUTTON, g.onButtonMash)
        # end debugging button

        self.SetSizer(sizer)
        sizer.Fit(self)

