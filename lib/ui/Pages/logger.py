# !/usr/bin/python3
#
# logger - Created 1/19/2018 6:31 PM
#
# Logging window used by app to capture event data.
#
# (c) 2017 - 2018 @ Grimmlabs
#
# See license file for redistribution details. 
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import logging
import wx

from ... import config as cfg


class WxTextCtrlHandler(logging.Handler):
    """Captures logging events from system logger and redirects to logging window in UI"""

    def __init__(self, ctrl):
        logging.Handler.__init__(self)
        self.ctrl = ctrl

    def emit(self, record):
        s = self.format(record) + '\n'
        wx.CallAfter(self.ctrl.AppendText, s)


class LoggerPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, name="loggr")

        sizer = wx.BoxSizer(wx.HORIZONTAL)

        self.logWindow = wx.TextCtrl(self, -1, style=wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH)
        self.logWindow.SetBackgroundColour(wx.BLACK)

        # make it mono font
        fn = self.logWindow.GetFont()
        fn.SetFamily(wx.FONTFAMILY_TELETYPE)
        self.logWindow.SetFont(fn)

        self.logWindow.SetForegroundColour(wx.WHITE)

        sizer.Add(self.logWindow, proportion=1, border=3, flag=wx.ALL | wx.EXPAND)

        handler = WxTextCtrlHandler(self.logWindow)

        log = logging.getLogger('gmoney')
        log.addHandler(handler)

        handler.setFormatter(logging.Formatter(fmt=cfg.shortformat, datefmt=cfg.dateformat))

        self.SetAutoLayout(True)
        self.SetSizer(sizer)
        sizer.Fit(self)
