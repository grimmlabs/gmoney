#
# See license file for redistribution details.
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import wx

from . import gridBase


class potGrid(gridBase.recipeGrid):
    def __init__(self, parent):
        gridBase.recipeGrid.__init__(self, parent, widgetName="potGrid", itemKind="pot", mult=1.5)


class phialGrid(gridBase.recipeGrid):
    def __init__(self, parent):
        gridBase.recipeGrid.__init__(self, parent, widgetName="phialGrid", itemKind="phial", mult=1.5)


class incenseGrid(gridBase.recipeGrid):
    def __init__(self, parent):
        gridBase.recipeGrid.__init__(self, parent, widgetName="incenseGrid", itemKind="incense", mult=1.5)


class potPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent,  name="potPanel")
        self.subs = []

        f = self.GetFont()
        f.SetWeight(wx.FONTWEIGHT_BOLD)
        f.SetPointSize(f.GetPointSize() + 2)

        sizer = wx.BoxSizer(wx.VERTICAL)

        t = wx.StaticText(self, -1, "Potions")
        t.SetFont(f)
        sizer.Add(t, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)

        g = potGrid(self)
        sizer.Add(g, proportion=1, border=3, flag=wx.ALL | wx.EXPAND)
        self.subs.append(g)

        t = wx.StaticText(self, -1, "Phials")
        t.SetFont(f)
        sizer.Add(t, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)

        g = phialGrid(self)
        sizer.Add(g, proportion=1, border=3, flag=wx.ALL | wx.EXPAND)
        self.subs.append(g)

        t = wx.StaticText(self, -1, "Incense")
        t.SetFont(f)
        sizer.Add(t, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)

        g = incenseGrid(self)
        sizer.Add(g, proportion=1, border=3, flag=wx.ALL | wx.EXPAND)
        self.subs.append(g)

#         # debugging button
#         # b = wx.Button(self, -1, label="Refresh")
#         # sizer.Add(b)
#         # b.Bind(wx.EVT_BUTTON, g.onButtonMash)
#         # end debugging button

        self.SetSizer(sizer)
        sizer.Fit(self)

    def refresh(self):
        for g in self.subs:
            wx.CallAfter(g.populate)
