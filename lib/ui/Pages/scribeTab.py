#
# (c) 2017 - 2020 @ Grimmlabs
#
# See license file for redistribution details. 
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import wx

from . import gridBase


class scribeMatGrid(gridBase.matGrid):
    def __init__(self, parent):
        gridBase.matGrid.__init__(self, parent, widgetName="scribeMatGrid", matKind="scribe-mats")


class optionalsGrid(gridBase.recipeGrid):
    def __init__(self, parent):
        gridBase.recipeGrid.__init__(self, parent, widgetName="optionalsGrid", itemKind="scribe-optionals")


class scribePanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, name='scribeTab')

        self.subs = []

        f = self.GetFont()
        f.SetWeight(wx.FONTWEIGHT_BOLD)
        f.SetPointSize(f.GetPointSize() + 2)

        sizer = wx.BoxSizer(wx.VERTICAL)

        t = wx.StaticText(self, -1, "Scribe Mats")
        t.SetFont(f)
        sizer.Add(t, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)

        g = scribeMatGrid(self)
        sizer.Add(g, proportion=1, border=3, flag=wx.ALL | wx.EXPAND)
        self.subs.append(g)

        l = wx.StaticLine(self)
        sizer.Add(l, border=5, flag=wx.ALL | wx.EXPAND)

        t = wx.StaticText(self, -1, "Optional Reagents")
        t.SetFont(f)
        sizer.Add(t, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)

        g = optionalsGrid(self)
        sizer.Add(g, proportion=1, border=3, flag=wx.ALL | wx.EXPAND)
        self.subs.append(g)

        # l = wx.StaticLine(self)
        # sizer.Add(l, border=5, flag=wx.ALL | wx.EXPAND)
        #
        # t = wx.StaticText(self, -1, "Metals")
        # t.SetFont(f)
        # sizer.Add(t, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)
        #
        # g = gemMetalGrid(self)
        # sizer.Add(g, proportion=1, border=3, flag=wx.ALL | wx.EXPAND)
        # self.subs.append(g)

        self.SetSizer(sizer)
        sizer.Fit(self)

    def refresh(self):
        for g in self.subs:
            wx.CallAfter(g.populate)
