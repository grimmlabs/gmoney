#
# (c) 2017 - 2020 @ Grimmlabs
#
# See license file for redistribution details. 
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import wx

from . import gridBase


class enchMatGrid(gridBase.matGrid):
    def __init__(self, parent):
        gridBase.matGrid.__init__(self, parent, widgetName="enchMatGrid", matKind="ench-mat")


class enchGrid(gridBase.recipeGrid):
    def __init__(self, parent):
        gridBase.recipeGrid.__init__(self, parent, widgetName="enchGrid", itemKind="ench", mult=1.1)


class enchPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent,  name="enchTab")

        sizer = wx.BoxSizer(wx.VERTICAL)
        self.subs =[]

        f1 = self.GetFont()
        f1.SetWeight(wx.FONTWEIGHT_BOLD)
        f1.SetPointSize(f1.GetPointSize() + 2)

        f2 = self.GetFont()
        f2.SetPointSize(f2.GetPointSize())

        t = wx.StaticText(self, -1, "Mats")
        t.SetFont(f1)
        sizer.Add(t, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)

        g = enchMatGrid(self)
        sizer.Add(g, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)
        self.subs.append(g)

        l = wx.StaticLine(self)
        sizer.Add(l, border=5, flag=wx.ALL | wx.EXPAND)

        t = wx.StaticText(self, -1, "Enchants")
        t.SetFont(f1)
        sizer.Add(t, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)

        g = enchGrid(self)
        sizer.Add(g, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)
        self.subs.append(g)

        t = wx.StaticText(self, -1, "Enchant costs are 1.1 base cost")
        t.SetFont(f2)
        sizer.Add(t, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)

        # debugging button
        # b = wx.Button(self, -1, label="Refresh")
        # sizer.Add(b)
        # b.Bind(wx.EVT_BUTTON, g.onButtonMash)
        # end debugging button

        self.SetSizer(sizer)
        sizer.Fit(self)

    def refresh(self):
        for g in self.subs:
            wx.CallAfter(g.populate)
