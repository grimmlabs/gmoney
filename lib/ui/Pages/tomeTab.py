#
# (c) 2017 - 2019 @ Grimmlabs
#
# See license file for redistribution details. 
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import wx

from . import gridBase


class tomeGrid(gridBase.recipeGrid):
    def __init__(self, parent):
        gridBase.recipeGrid.__init__(self, parent, widgetName="tomeGrid", itemKind="tome", mult=1.5)

    def getName(self, raw):
        """
        Override default getName()
        """
        _, name = raw.split(": ")
        return name


class tomeMatsGrid(gridBase.matGrid):
    def __init__(self, parent):
        gridBase.matGrid.__init__(self, parent, widgetName="tomeMatsGrid", matKind="tome-mats")


class tomePanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent,  name="tomeTab")
        self.subs = []

        f = self.GetFont()
        f.SetWeight(wx.FONTWEIGHT_BOLD)
        f.SetPointSize(f.GetPointSize() + 2)

        sizer = wx.BoxSizer(wx.VERTICAL)

        t = wx.StaticText(self, -1, "Tomes")
        t.SetFont(f)
        sizer.Add(t, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)

        g = tomeGrid(self)
        sizer.Add(g, proportion=1, border=3, flag=wx.ALL | wx.EXPAND)
        self.subs.append(g)

        t = wx.StaticText(self, -1, "Listed cost is 1.5 actual")
        sizer.Add(t, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)

        l = wx.StaticLine(self)
        sizer.Add(l, border=5, flag=wx.ALL | wx.EXPAND)

        t = wx.StaticText(self, -1, "Tome Mats")
        t.SetFont(f)
        sizer.Add(t, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)

        g = tomeMatsGrid(self)
        sizer.Add(g, proportion=1, border=3, flag=wx.ALL | wx.EXPAND)
        self.subs.append(g)

        # debugging button
        # b = wx.Button(self, -1, label="Refresh")
        # sizer.Add(b)
        # b.Bind(wx.EVT_BUTTON, g.onButtonMash)
        # end debugging button

        self.SetSizer(sizer)
        sizer.Fit(self)

    def refresh(self):
        for g in self.subs:
            wx.CallAfter(g.populate)
