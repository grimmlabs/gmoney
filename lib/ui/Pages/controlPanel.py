# !/usr/bin/python3
#
# controlPanel
#
# Control panel
#
# See license file for redistribution details.
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import webbrowser
import wx


class Control(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, name="controlPanel")

        self.prefs = wx.GetApp().prefs

        sizer = wx.GridBagSizer(vgap=3, hgap=3)

        # b = wx.Button(self, label="Open Window")
        # sizer.Add(b, (0, 0), (1, 1), border=3, flag=wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_TOP)
        # b.Bind(wx.EVT_BUTTON, self.onButtonMash)

        cb = wx.CheckBox(self, label="Debug")
        cb.SetValue(self.prefs['debug'])
        sizer.Add(cb, (1, 0), (1, 1), border=3, flag=wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_TOP)
        cb.Bind(wx.EVT_CHECKBOX, self.toggleDebug)

        self.SetAutoLayout(True)
        self.SetSizer(sizer)
        sizer.Fit(self)

    # def onButtonMash(self, event):
    #     """Handle button mash"""
    #     if event.Id == 1003:
    #         # show gems page
    #         webbrowser.open_new("http://localhost:9090/gems")
    #     else:
    #         # fall through to main page
    #         webbrowser.open_new("http://localhost:9090")

    def toggleDebug(self, event):
        if self.prefs['debug']:
            # print("turning off debug")
            self.prefs['debug'] = False
        else:
            # print("turning on debug")
            self.prefs['debug'] = True
