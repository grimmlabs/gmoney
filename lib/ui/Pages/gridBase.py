#
# (c) 2017 - 2019 @ Grimmlabs
#
# See license file for redistribution details. 
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import json

import wx
import wx.grid as gridLib
import wx.lib.mixins.gridlabelrenderer as glr
from  humanize.number import intcomma

from ... import config as cfg


class labelGridRenderer(glr.GridLabelRenderer):
    def __init__(self):
        self._bgcolor = "#000000"
        self._fgcolor = wx.Colour(255, 255, 255)

    def Draw(self, grid, dc, rect, col):
        dc.SetBrush(wx.Brush(self._bgcolor))
        dc.SetPen(wx.TRANSPARENT_PEN)
        dc.DrawRectangle(rect)
        hAlign, vAlign = grid.GetColLabelAlignment()
        text = grid.GetColLabelValue(col)
        self.DrawBorder(grid, dc, rect)
        grid.SetLabelTextColour(self._fgcolor)
        self.DrawText(grid, dc, rect, text, hAlign, vAlign)


class priceGrid(gridLib.Grid, glr.GridWithLabelRenderersMixin):
    numColumns = 4
    colLabels = ["Item", "Buyout", "Historical", "Auctions"]
    mult=1

    def __init__(self, parent, name="", kind=""):
        gridLib.Grid.__init__(self, parent, name=name)
        glr.GridWithLabelRenderersMixin.__init__(self)

        self.kind = kind
        self.items = []
        self.parent = parent

        self.font = fn = wx.Font(
            10, wx.FONTFAMILY_MODERN, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, faceName="Consolas")

        self.colRenderer = labelGridRenderer()
        self.initColumns()
        self.HideRowLabels()

        self.SetColLabelAlignment(horiz=wx.ALIGN_CENTER, vert=wx.ALIGN_BOTTOM)
        self.populate(initialize=True)
        self.AutoSizeColumns()

    def initColumns(self):
        self.CreateGrid(0, self.numColumns)

        for i in range(0, self.numColumns):
            self.SetColLabelValue(i, self.colLabels[i])

        for col in range(0, self.numColumns):
            self.SetColLabelRenderer(col, self.colRenderer)

    def populate(self, initialize=False):
        """You need to put your own method in for this."""
        raise NotImplemented

    def GetItemQuality(self, item):
        qualityTypes = {
            'uncommon': "#006400",
            'rare': "#0000FF",
            'epic': "#800080",
            'legendary': "#FF4500",
        }

        return qualityTypes.get(item['qual'], wx.BLACK)

    def formatPrice(self, rawPrice):
        """Return a string formatted as gold / silver / copper like you see in-game in WoW."""
        if rawPrice == -1:
            return '-'

        p = str(int(rawPrice))

        c = p[-2:]
        s = p[-4:-2]
        g = p[:-4]

        rc = ""

        if g:
            rc += f'{intcomma(g)}g'
        else:
            rc += '0g'

        if s:
            rc += f' {s}s'
        else:
            rc += ' 0s'

        if c:
            rc += f' {c}c'
        else:
            rc += ' 0c'

        return rc

    def judgement(self, candidate, reference):
        if candidate > reference:
            return wx.WHITE, wx.RED
        elif candidate < reference:
            return wx.WHITE, "#006400"
        else:
            return wx.BLACK, wx.WHITE

    def getAuctionData(self, id, auctions):
        for a in auctions['auctions']:
            if a['id'] == id:
                return a

        return None

    def getCost(self, data=None, kind=""):
        """Calculate the cost of creating an item"""
        with open(cfg.recipeFile) as fp:
            recipes = json.load(fp)

        if kind not in recipes:
            return 0

        recipe = recipes[kind]

        totalCost = 0

        for item, qty, name in recipe:
            d = self.getAuctionData(item, data)

            if d:
                totalCost += (qty * d['hist'])

        return totalCost * self.mult

    def getName(self, raw):
        return raw[:]

    def onButtonMash(self, evt):
        """debugging button"""
        print("mashed")
        self.populate()


class matGrid(priceGrid):
    """
    A grid that shows a list of items used in recipes or sold directly (commodities)
    """
    numColumns = 4
    colLabels = ["Item", "Buyout", "Historical", "Auctions"]

    def __init__(self, parent, widgetName="", matKind=""):
        priceGrid.__init__(self, parent, name=widgetName, kind=matKind)

    def populate(self, initialize=False):
        if not initialize:
            r = self.GetNumberRows()

            if r:
                self.DeleteRows(0, r)

        with open(cfg.displayFile) as fp:
            jdata = json.load(fp)

        self.items = []

        for i in jdata['auctions']:
            if self.kind in i['kind']:
                self.items.append(i)

        for i in self.items:
            idx = self.items.index(i)
            self.AppendRows(numRows=1)

            name = self.getName(i['name'])

            self.SetCellValue(idx, 0, name)
            self.SetCellAlignment(idx, 0, wx.ALIGN_RIGHT, wx.ALIGN_TOP)
            self.SetCellTextColour(idx, 0, self.GetItemQuality(i))
            self.SetCellFont(idx, 0, self.font.Bold())

            self.SetCellValue(idx, 1, self.formatPrice(i['buyout']))
            self.SetCellAlignment(idx, 1, wx.ALIGN_RIGHT, wx.ALIGN_TOP)
            self.SetCellFont(idx, 1, self.font)

            formatting = self.judgement(i['buyout'], i['hist'])
            self.SetCellTextColour(idx, 1, formatting[0])
            self.SetCellBackgroundColour(idx, 1, formatting[1])

            self.SetCellValue(idx, 2, self.formatPrice(i['hist']))
            self.SetCellAlignment(idx, 2, wx.ALIGN_RIGHT, wx.ALIGN_TOP)
            self.SetCellFont(idx, 2, self.font)

            self.SetCellValue(idx, 3, str(intcomma(i['qty'])))
            self.SetCellAlignment(idx, 3, wx.ALIGN_RIGHT, wx.ALIGN_TOP)
            self.SetCellFont(idx, 3, self.font)


class recipeGrid(priceGrid):
    """
    A grid that shows a list of recipe'd items along with creation cost
    """
    numColumns = 5
    colLabels = ["Item", "Cost", "Buyout", "Historical", "Auctions"]

    def __init__(self, parent, widgetName="", itemKind="", mult=1):
        priceGrid.__init__(self, parent, name=widgetName, kind=itemKind)
        self.mult=mult

    def populate(self, initialize=False):
        if not initialize:
            r = self.GetNumberRows()

            if r:
                self.DeleteRows(0, r)

        with open(cfg.displayFile) as fp:
            jdata = json.load(fp)

        self.items = []

        for i in jdata['auctions']:
            if self.kind in i['kind']:
                self.items.append(i)

        for i in self.items:
            idx = self.items.index(i)
            self.AppendRows(numRows=1)

            name = self.getName(i['name'])

            self.SetCellValue(idx, 0, name)
            self.SetCellAlignment(idx, 0, wx.ALIGN_RIGHT, wx.ALIGN_TOP)
            self.SetCellTextColour(idx, 0, self.GetItemQuality(i))
            self.SetCellFont(idx, 0, self.font.Bold())

            cost = self.getCost(data=jdata, kind=i['recipe'])
            self.SetCellValue(idx, 1, self.formatPrice(cost))
            self.SetCellAlignment(idx, 1, wx.ALIGN_RIGHT, wx.ALIGN_TOP)
            self.SetCellFont(idx, 1, self.font)

            formatting = self.judgement(cost, i['hist'])
            self.SetCellTextColour(idx, 1, formatting[0])
            self.SetCellBackgroundColour(idx, 1, formatting[1])

            self.SetCellValue(idx, 2, self.formatPrice(i['buyout']))
            self.SetCellAlignment(idx, 2, wx.ALIGN_RIGHT, wx.ALIGN_TOP)
            self.SetCellFont(idx, 2, self.font)

            formatting = self.judgement(i['buyout'], i['hist'])
            self.SetCellTextColour(idx, 2, formatting[0])
            self.SetCellBackgroundColour(idx, 2, formatting[1])

            self.SetCellValue(idx, 3, self.formatPrice(i['hist']))
            self.SetCellAlignment(idx, 3, wx.ALIGN_RIGHT, wx.ALIGN_TOP)
            self.SetCellFont(idx, 3, self.font)

            self.SetCellValue(idx, 4, str(intcomma(i['qty'])))
            # self.SetCellValue(idx, 4, str(i['qty']))
            self.SetCellAlignment(idx, 4, wx.ALIGN_RIGHT, wx.ALIGN_TOP)
            self.SetCellFont(idx, 4, self.font)
