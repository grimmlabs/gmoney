#
# (c) 2017 - 2020 @ Grimmlabs
#
# See license file for redistribution details. 
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import wx

from . import gridBase


class clothMatsGrid(gridBase.matGrid):
    def __init__(self, parent):
        gridBase.matGrid.__init__(self, parent, widgetName="clothGrid", matKind="cloth")


class tailorGrid(gridBase.recipeGrid):
    def __init__(self, parent):
        gridBase.recipeGrid.__init__(self, parent, widgetName="tailorGrid", itemKind="tailor", mult=1.1)


class clothPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, name='tailorTab')
        self.subs = []

        f = self.GetFont()
        f.SetWeight(wx.FONTWEIGHT_BOLD)
        f.SetPointSize(f.GetPointSize() + 2)

        sizer = wx.BoxSizer(wx.VERTICAL)

        l = wx.StaticLine(self)
        sizer.Add(l, border=5, flag=wx.ALL | wx.EXPAND)

        t = wx.StaticText(self, -1, "Cloth Mats")
        t.SetFont(f)
        sizer.Add(t, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)

        g = clothMatsGrid(self)
        sizer.Add(g, proportion=1, border=3, flag=wx.ALL | wx.EXPAND)
        self.subs.append(g)

        l = wx.StaticLine(self)
        sizer.Add(l, border=5, flag=wx.ALL | wx.EXPAND)

        t = wx.StaticText(self, -1, "Tailoring")
        t.SetFont(f)
        sizer.Add(t, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)

        g = tailorGrid(self)
        sizer.Add(g, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)
        self.subs.append(g)

        f2 = self.GetFont()
        f2.SetPointSize(f2.GetPointSize())

        t = wx.StaticText(self, -1, "Enchant costs are 1.1 base cost")
        t.SetFont(f2)
        sizer.Add(t, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)

        self.SetSizer(sizer)
        sizer.Fit(self)

    def refresh(self):
        for g in self.subs:
            wx.CallAfter(g.populate)
