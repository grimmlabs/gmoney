#
# (c) Grimmlabs
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import logging.config

from . import config as cfg


def init():
    logging.config.dictConfig(cfg.logConfig)
    log = logging.getLogger(cfg.logName)
    return log
