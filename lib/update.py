#
# update - Created 8/24/2017 11:16 PM
#
# Stuff that updates the local database.
#
# (c) 2017 - 2020 @ Grimmlabs
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import logging
import json

from threading import *

import wx

from . import blizz
from . import config as cfg
from . import history
from . import ofInterest

log = logging.getLogger(cfg.logName)
EVT_PROCESS_RESULT_ID = wx.NewEventType()


class UpdateEvent(wx.PyEvent):
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_PROCESS_RESULT_ID)


def process(win, updated):
    log.info(" --> Processing raw data")
    intermediate = {}

    # win.dlg = wx.ProgressDialog("Importing", f"Importing 1/{len(updated)}", maximum=len(updated), parent=win,
    #                             style=wx.PD_AUTO_HIDE).Show()

    for a in updated:
        log.debug(f"Collecting item {a['item']}")
        current = updated.index(a) + 1
        # win.dlg.Update(current, newmsg=f"Importing {current}/{len(updated)}")

        buyout = 0

        if 'unit_price' in a:
            buyout = a['unit_price']
        elif 'buyout' in a:
            buyout = a['buyout']
        else:
            log.error(f"*** ERROR - unknown pricing type for item id {a['item']}")

        if a['item'] not in intermediate:
            new = {
                'qty': a['quantity'],
                'min': buyout,
                'max': buyout,
                'qual': blizz.getItemQuality(a['item']),
                'buyouts': [buyout, ],
                'kind': a['kind']
            }

            intermediate[a['item']] = new
        else:
            old = intermediate[a['item']]
            old['qty'] += a['quantity']
            old['min'] = min(old['min'], buyout)
            old['max'] = max(old['max'], buyout)
            old['buyouts'].append(buyout)

    processed = {}
    # win.dlg.Destroy()

    for key, value in intermediate.items():
        log.debug(f"Processing {key}")

        processed[key] = {
            'qty': value['qty'],
            'qual': value['qual'],
            'buyout': min(value['buyouts']),
            'kind': value['kind']
        }

        history.addHistory(key, processed[key], win)

    return processed


def sortFunc(a):
    return a['name']


def populate(raw, win):
    log.info(" --> Filling in display file ...")

    final = []

    for item in ofInterest.itemsOfInterest():
        log.debug(f"---> Serializing item [{item['desc']}]")
        hist = history.historicalAverage(item['id'], win.historyDB)
        itemName = blizz.getItemDescription(item['id'])

        if item['id'] in raw:
            myData = raw[item['id']]
            myData['name'] = itemName

            if myData['buyout'] < hist:
                myData['judgement'] = 'l'
            elif myData['buyout'] > hist:
                myData['judgement'] = 'h'
            else:
                myData['judgement'] = 'n'

            myData['hist'] = hist
            myData['id'] = item['id']
            myData['recipe'] = item.get('recipe', "")
        else:
            myData = {
                'name': itemName,
                'buyout': -1,
                'judgement': 'n',
                'hist': hist,
                'id': item['id'],
                'qty': 0,
                'kind': item['kind'],
                'qual': blizz.getItemQuality(item['id']),
                'recipe': item.get('recipe', "")
            }

        final.append(myData)

    liveData = {
        'auctions': final,
    }

    with open(cfg.displayFile, "w") as fp:
        json.dump(liveData, fp, indent=4)

    log.info(" --> Display file updated.")

    history.saveHistory(win.historyDB)

    log.info("****************")
    log.info("Update Complete.")
    log.info("****************")


class ProcessingThread(Thread):
    def __init__(self, win, updated):
        Thread.__init__(self)
        self.win = win
        self.updated = updated
        self._want_abort = False
        self.start()

    def run(self):
        if self._want_abort:
            wx.PostEvent(self.win, UpdateEvent())
            return

        p = process(self.win, self.updated)
        populate(p, self.win)
        wx.PostEvent(self.win, UpdateEvent())

    def abort(self):
        self._want_abort = True


def do_update(win, updated):
    if not win.worker:
        win.worker = ProcessingThread(win, updated)
