#
# ofInterest - Created 9/3/2017 10:32 AM
#
# Load config file listing auctions I am interested in.
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import json
import logging

from . import config as cfg

log = logging.getLogger(cfg.logName)


def itemsOfInterest():
    rc = []

    with open("of-interest.json") as fp:
        items = json.load(fp)

    for i in items:
        if 'enabled' in i:
            if not i['enabled']:
                # log.debug(f"Not loading disabled item {i['id']} ({i['desc']})")
                continue

        rc.append(i)

    return rc


def getKind(itemNo):
    for item in itemsOfInterest():
        if item['id'] == itemNo:
            return item['kind']

    return None


def getItem(itemNo):
    """Retrieve item of interest from list of items of interest"""

    for item in itemsOfInterest():
        if item['id'] == itemNo:
            return item

    return None
