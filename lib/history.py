#
# history - Created 9/3/2017 11:33 AM
#
# In which historical data is dealt with.
#
# (c) 2017 - 2020 @ Grimmlabs
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import datetime
import json
import logging
import statistics

from . import config as cfg
from . import ofInterest

log = logging.getLogger(cfg.logName)


def addHistory(itemKey, data, win):
    """Add an item to the history database."""

    key = str(itemKey)

    entry = [datetime.datetime.now().strftime('%Y-%m-%d'), data['buyout']]

    itemInfo = ofInterest.getItem(itemKey)

    # if 'track' in itemInfo and itemInfo['track'] == True:
    #     log.info(f"Trackable object [{itemKey}]")

    if key in win.historyDB:
        win.historyDB[key].append(entry)
    else:
        win.historyDB[key] = [entry, ]


def historicalAverage(itemKey, historyDB):
    """Returns the historical average for an item."""
    itemList = historyDB.get(str(itemKey), None)

    if not itemList:
        log.error(f"*** historicalAverage(): Did not find item [{itemKey}] in historical database.")
        return 0

    hlist = [item for dtg, item in itemList]
    return statistics.median(hlist)


def saveHistory(historyDB):
    """Saves history to disk."""

    log.info(" --> Saving historyDB")
    log.debug(f"     ... historyDB contains {len(historyDB)} entries.")

    today = datetime.datetime.today().date()
    cutOffDate = today - datetime.timedelta(days=cfg.historyAge)

    newHistoryDB = {}
    total = 0
    totalDropped = 0

    for item, data in historyDB.items():

        newData = []

        for dtg, entry in data:

            year, mon, day = dtg.split('-')
            ts = datetime.date(int(year), int(mon), int(day))

            if ts > cutOffDate:
                newData.append([dtg, entry])
            else:
                totalDropped += 1

        newHistoryDB[item] = newData
        total += len(newData)

    with open(cfg.historyDB, "w") as fp:
        json.dump(newHistoryDB, fp, indent=4)

    if totalDropped:
        log.info(f" ---> {totalDropped} old historical database entries dropped.")

    log.info(f" --> History DB ({total} items) saved to disk.")
