#
# blizz - Created 9/3/2017 10:39 AM
#
# Stuff that hits blizzard interface
#
# (c) 2017 - 2020 @ Grimmlabs
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import logging

from threading import *

import requests
import wx

from .ofInterest import itemsOfInterest, getKind

from . import config as cfg

log = logging.getLogger(cfg.logName)
EVT_BLIZZ_RESULT_ID = wx.NewEventType()


class BlizzEvent(wx.PyEvent):
    def __init__(self, eventData):
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_BLIZZ_RESULT_ID)
        self.data = eventData


class GetBlizzThread(Thread):
    def __init__(self, win):
        Thread.__init__(self)
        self.win = win
        self._want_abort = False
        self.start()

    def run(self):
        if self._want_abort:
            wx.PostEvent(self.win, BlizzEvent(None))
            return

        log.info("Updating from Blizz ...")

        try:
            r = requests.post(
                f"{cfg.bnetOauthURL}?grant_type=client_credentials", auth=(cfg.clientID, cfg.clientSecret)
            )
        except requests.exceptions.ConnectionError:
            log.info("Blizzard connection failed during token retrieval (recoverable).")
            wx.PostEvent(self.win, BlizzEvent(None))
            return

        rc = []

        if r.status_code == 200:
            j = r.json()
            self.win.access_token = j['access_token']
        else:
            log.error("Unable to obtain access token.")
            wx.PostEvent(self.win, BlizzEvent(None))
            return

        # Now get realm data
        try:
            r = requests.get(
                f"{cfg.remoteBaseURL}/data/wow/realm/alleria?namespace=dynamic-us&locale=en_US&"
                f"access_token={self.win.access_token}"
            )
        except requests.exceptions.ConnectionError as e:
            log.error("**** Connection to host failed. ****")
            log.error(repr(e))
            wx.PostEvent(self.win, BlizzEvent(None))
            return

        if not r.status_code == 200:
            log.info("Unable to retrieve realm data.")
            wx.PostEvent(self.win, BlizzEvent(None))
            return

        realmData = r.json()

        # Only pull data if it's been modified.
        if self.win.lastModified:
            headers = {
                'If-Modified-Since': self.win.lastModified
            }
        else:
            headers = {}

        # Finally we can get AH data
        try:
            r = requests.get(
                f"{cfg.remoteBaseURL}/data/wow/connected-realm/{realmData['id']}/auctions?namespace=dynamic-us&"
                f"locale=en_US&access_token={self.win.access_token}",
                headers=headers
            )

        except requests.exceptions.ConnectionError:
            log.info("Blizzard connection failed (recoverable).")
            wx.PostEvent(self.win, BlizzEvent(None))
            return

        except requests.exceptions.ReadTimeout:
            log.error("Blizz connection timed out retrieving AH data. Recovering.")
            wx.PostEvent(self.win, BlizzEvent(None))

        if r.status_code == 200:
            j = r.json()
            auctions = j['auctions']
            log.info(f"{len(auctions)} auctions retrieved.")

            log.info("Retrieving commodity data.")
            commodityURL = j['commodities']['href']
            r2 = requests.get(f"{commodityURL}&access_token={self.win.access_token}")

            if r2.status_code == 200:
                commodityData = r2.json()
                log.info(f"{len(commodityData['auctions'])} commodity auctions retrieved.")
                auctions = auctions + commodityData['auctions']

            self.win.lastModified = r.headers['Last-Modified']

            log.info("********************")
            log.info("New data, processing")
            log.info("********************")

            itemNumbers = [item['id'] for item in itemsOfInterest()]
            itemNumbers.sort()

            # Flatten the structure
            for a in auctions:
                i = a['item']['id']
                a['item'] = i

            for a in auctions:
                if a['item'] in itemNumbers:
                    a['kind'] = getKind(a['item'])
                    rc.append(a)

            log.info(f" --> {len(rc)} items retrieved.")
            wx.PostEvent(self.win, BlizzEvent(rc))

        elif r.status_code == 304:
            log.info("... Data unchanged on server.")
            wx.PostEvent(self.win, BlizzEvent(None))
            return
        else:
            log.error(f"Unknown eror [{r.status_code}]")
            wx.PostEvent(self.win, BlizzEvent(None))
            return

    def abort(self):
        self._want_abort = True


def updateFromBlizz(caller):
    if not caller.worker:
        caller.worker = GetBlizzThread(caller)


def getBlizzToken():
    return wx.FindWindowByName("gmoneyTopFrame").access_token


def getItemDescription(item):
    MainWin = wx.FindWindowByName("gmoneyTopFrame")

    rc = MainWin.itemCache.get(item, "")

    if rc:
        log.debug("... Cache hit.")
        return rc['name']

    r = requests.get(
        f"{cfg.remoteBaseURL}/data/wow/item/{item}?namespace=static-us&locale=en_US&access_token={getBlizzToken()}"
    )

    if r.status_code == 200:
        j = r.json()
        rc = j['name']
        MainWin.updateCache(j)
    else:
        rc = "Error Response from Blizzard"

    return rc


def getItemQuality(item):
    """Get item quality from Blizz"""

    MainWin = wx.FindWindowByName("gmoneyTopFrame")

    rc = MainWin.itemCache.get(item, "")

    if rc:
        log.debug("... Cache hit.")
        return rc['quality']['name'].lower()

    try:
        r = requests.get(
            f"{cfg.remoteBaseURL}/data/wow/item/{item}?namespace=static-us&locale=en_US&access_token={getBlizzToken()}"
        )
    except requests.exceptions.ConnectionError as e:
        log.error("**** Connection to host failed (getItemQuality). ****")
        log.error(repr(e))
        rc = 'common'
        return rc

    if r.status_code == 200:
        j = r.json()
        rc = j['quality']['name'].lower()
        MainWin.updateCache(j)
    else:
        rc = 'common'

    return rc
