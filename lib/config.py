#
# (c) Grimmlabs
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import logging
import os
import sys

sys.path.insert(0, 'lib')
cwd = os.getcwd()

displayFile = "display.json"
recipeFile = "recipes.json"
historyDB = "history.json"
historyAge = 36
cacheFile = "cache.json"

# blizz
bnetOauthURL = "https://us.battle.net/oauth/token"
remoteBaseURL = "https://us.api.blizzard.com"
clientID = "24e2585bdea24a72a133f8230dc7e53b"
clientSecret = "fhFs1wTJwAH4dnslDNjWSi7ijFcgQGHa"
refreshInterval = 15  # minutes

# wx
vendorName = 'Grimmlabs'
geometryFile = f'{cwd}/ggeometry.json'

# logging
logName = 'gmoney'
dateformat = "%Y-%m-%d %H:%M:%S"
shortformat = "%(asctime)s %(levelname)1.1s> %(message)s"

logConfig = {
    'version' : 1,
    "disable_existing_loggers": True,

    'loggers': {
        logName: {
            # 'level': logging.DEBUG,
            'level': logging.INFO,
            "handlers": ['console', 'logfile',],
            'propagate': True,
            'qualname': logName
        },
    },

    'handlers': {
        'console': {
            "class": 'logging.StreamHandler',
            "formatter": "brief",
        },
        'logfile': {
            "class": 'logging.FileHandler',
            'formatter': 'brief',
            'filename': f"{logName}.log",
            'mode': "w",
        }
    },

    'formatters': {
        'default':
            {
                'format': "%(asctime)s %(levelname)1.1s> %(message)s (%(module)-15s:%(lineno)d)",
                'datefmt': dateformat,
            },
        'brief':
            {
                'format' : shortformat,
                'datefmt': dateformat,
            },
    }
}
