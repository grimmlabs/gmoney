#
# Version info.
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

major       = 6
minor       = 1
rev         = 4

name        = "gMoney"
splash      = "gMoney - Behold the magnificence of our turbulence"

version     = f'{major}.{minor:02}.{rev:02}'
fullVersion = f'{name} {version} (c) Grimmlabs'

author = "Jeff Grimmett (grimmtooth@gmail.com)"


#
# Changelog
#
# 6.01.04   Adding alchemy
# 6.01.03   Starting with the Ench stuff
# 6.01.02   Fix startup error
# 6.01.01   Added SL herbs back.
# 6.01.00   Dragonflight is live!
# 6.00.02   Reverted First Flower
# 6.00.01   Reverted removal of herbs until after the expansion
# 6.00.00   Pre-patch levels of fun (10.0)
# 5.01.00   Adding new commodity endpoint
# 5.01.06   Adding 9.2 mats
# 5.01.05   Attempting to handle timeout error w/Blizz
# 5.01.04   Added tailoring 32-slot bags
#           Added main frame icon
# 5.01.03   Updated enchanting, added enchanted cloth
# 5.01.02   Now runs without a shell window. Took this long because I'm an idiot.
# 5.01.01   Updated enchanting
# 5.01.00   Serialize historical data to SQL database for visualization
# 5.00.16   Updated enchanting
# 5.00.15   Change OAUTH token retrieval method from GET to POST
# 5.00.14   Updated alchemy recipes
# 5.00.13   Updated enchanting window
# 5.00.12   Added some alchemy recipes, some text to the tomes tab
# 5.00.11   Additional Enchanting recipes
# 5.00.10   Additional Enchanting recipes
# 5.00.09   Fix to enchanting recipes not showing correct cost.
#           Fix to refreshing of tabs on first cycle
# 5.00.08   Enchanting recipes
# 5.00.07   Adding some Enchanting stuff, updating JC
#           Added scribe tab
#           Adding comments to recipies now.
# 5.00.06   Correction for prices for jc products
#           Refactoring grid panels to make them easier to maintain
#           Humanized auction quantity in grids
# 5.00.05   Additional mats added, added actual jc products
# 5.00.04   Updated JC mats some more
# 5.00.03   Added herbs for SL, removed BfA herbs
# 5.00.02   Updates to the gemcrafting tab
# 5.00.01   Added cloth tab
# 5.00.00   Shadowlands
# 4.05.01   Updated Blizz function to get token after API change
# 4.05.00   Updated wxPython and oops that broke a bunch of shit
# 4.04.02   Fix a potential crash that could happen.
# 4.04.01   Humanize large numbers in grid displays
# 4.04.00   Added a dialog for the import phase
# 4.03.01   PyInstaller builds now work - we can create EXEs!
#           Unfortunately, '--onefile' seems broken so it's not that great.
# 4.03.00   Use headers to determine if new data is present.
# 4.02.01   Turn off debugging in log
# 4.02.00   Added on-disk cache for item info so we don't have to fetch from Blizz every time
# 4.01.02   Clean up data misrepresentations.
# 4.01.01   Blizz killed the community API; jiggered for the new Game Data APIs. Augh.
# 4.01.00   History db now pretty-prints.
# 4.00.03   Backed out some changes for grid tab refreshes
# 4.00.02   Removed Twisted logging and fixed refreshes on grid tabs
# 4.00.01   Removing stuff linked to old webserver based app
# 4.00.00   Corrections for many flawed assumptions in v3.
# 3.07.02   Emergency change to get historyDB working again
# 3.07.01   Updates to get pyInstaller working
# 3.07.00   Create an installable tool
#           Removed Twisted and instead switching to asyncio for our main event loop
# 3.06.01   (JC) Removed kraken's eye, replaced with leviathan's eye
# 3.06.00   Added Tome Mats to Tome tab
#           Cleaning up duplicate functions
# 3.05.01   Removed green gems
# 3.05.00   Combined Enchanting tabs
# 3.04.01   Removed old button menu code tied to web pages
# 3.04.00   Removed some redundant code
# 3.03.01   Removed WebPanel.
#           Dismantled webserver.
# 3.03.00   Add Enchant mats
#           Add Flasks
#           Add 8.2 Ring and Weapon enchants
#           Removed active web content (except for jc)
#           Moved JC to a grid tab
# 3.02.01   Correct tome cost price (mult * 1.5)
# 3.02.00   Adding "Tome" tab
# 3.01.00   Adding "Misc" tab
#           Moved Dreamleaf to Misc tab
#           Moved debug flag to Control panel (from config file)
# 3.00.03   Some possibly vain attempts to get the grid windows to update when we get updated data from Blizz
# 3.00.02   Refactor herb panel into a generic + herb panel that can be shared
#           Added version to statusbar pane 0, moved last update to pane 1
#           Added means to load once use many on display file data
# 3.00.01   Move "last updated" to frame status bar
# 3.00.00   Add Grid view for BfA herbs
# 2.03.01   Added recipes for 8.02 patch
#           Removed low-end recipes that aren't ever going to sell.
# 2.03.00   Added "gem" panel to native UI
# 2.02.02   Fixed a typo in the gems page
# 2.02.00   Add gems tab
# 2.01.00   Added Tray Icon menu entry to open the web page directly
# 2.00.05   Configuration parameter for debug mode
#           Reduced time period to 48 days (4 weeks) instead of 8 weeks.
#           Added window w/button to show interface.
# 2.00.04   Added favicon
# 2.00.03   Added JC epic gems to tracking
# 2.00.02   Recipes now use historical price over current buyout price for more stable calculations.
# 2.00.01   Runs as startup script
# 2.00.00   Switch to wxreactor
# 1.06.02   Tidy up flask names
# 1.06.01   Added flasks
# 1.06.00   Moving big blocks into extention templates
# 1.05.01   Added low-end ring enchants
# 1.05.00   Execute update function as a background thread. Not 100% safe but I don't have 1000s of users hitting
#           this thing, so I can handle the risk :)
# 1.04.04   Changed cost to highlight background instead of foreground because that's more visible.
# 1.04.03   Enchants table
# 1.04.02   Defined abbreviated tome table (the big tables are too much info, not useful enough)
# 1.04.01   Twisted server wasn't loading css correctly. Fixed.
# 1.04.00   Fully changed to new blizz API
# 1.03.01   Changed to new Blizzard API
# 1.03.00   Dropped Flask support in favor of pure Twisted
# 1.02.00   Windows-friendly
#           * Output to a window not stdout
#           * Runs at startup and iconifies to system tray
#           * Changed name. It isn't a 'sniper'.
# 1.01.00   BfA
# 1.00.00   First cut
#

