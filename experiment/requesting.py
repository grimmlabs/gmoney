#
# requests - Created 7/25/2021 11:46 AM
# Testing request-related stuff.
#
# Author: Jeff Grimmett (jeffreyg3@verifone.com)
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79
###########################################################################

import sys
import requests

bnetOauthURL = "https://us.battle.net/oauth/token"
clientID = "24e2585bdea24a72a133f8230dc7e53b"
clientSecret = "fhFs1wTJwAH4dnslDNjWSi7ijFcgQGHa"
remoteBaseURL = "https://us.api.blizzard.com"

print("Requesting r1")
r1 = requests.post(f"{bnetOauthURL}?grant_type=client_credentials", auth=(clientID, clientSecret))

if r1.status_code == 200:
    j1 = r1.json()
    access_token = j1['access_token']
else:
    print("Error getting r1.")
    sys.exit(10)

print("Requesting r2")
r2 = requests.get(f"{remoteBaseURL}/data/wow/realm/alleria?namespace=dynamic-us&locale=en_US&access_token={access_token}")

if not r2.status_code == 200:
    print("Error getting r2")
    sys.exit(10)

realmData = r2.json()

print("requesting r3")
r3 = requests.get(
        f"{remoteBaseURL}/data/wow/connected-realm/{realmData['id']}/auctions?namespace=dynamic-us&"
        f"locale=en_US&access_token={access_token}")

if not r3.status_code == 200:
    print("Error getting r3")
    sys.exit(10)

j3 = r3.json()
commodityURL = j3['commodities']['href']

print("requesting r4")
r4 = requests.get(f"{commodityURL}&access_token={access_token}")

if not r4.status_code == 200:
    print("Error getting r4")
    sys.exit(10)

commodityData = r4.json()

print(None)
